FROM jenkins:1.625.2
MAINTAINER Bernd Fischer <bfischer@mindapproach.de>

###################################################

COPY plugins.txt    /usr/share/jenkins/plugins.txt
RUN /usr/local/bin/plugins.sh /usr/share/jenkins/plugins.txt

###

COPY hudson.tasks.Maven.xml /usr/share/jenkins/ref/hudson.tasks.Maven.xml

RUN mkdir -p /usr/share/jenkins/ref/.m2
COPY maven-settings.xml /usr/share/jenkins/ref/.m2/settings.xml

###

ENV DOCKER_CLIENT_CONFIG_PATH /etc/docker-client
ENV DOCKER_CLIENT_CERT_PATH   $DOCKER_CLIENT_CONFIG_PATH/certs
ENV DOCKER_CERT_PATH          $DOCKER_CLIENT_CERT_PATH

USER root

RUN  mkdir -p $DOCKER_CLIENT_CERT_PATH

RUN chown -R jenkins $DOCKER_CLIENT_CONFIG_PATH
RUN chown -R jenkins $DOCKER_CLIENT_CONFIG_PATH/*

COPY docker-1.6-linux /usr/local/bin/docker
RUN  chmod +x /usr/local/bin/docker
RUN  chown jenkins /usr/local/bin/docker

USER jenkins
